const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

// Route for check email
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

	
		if (result.length > 0) {
			return true;

		} else {
			return false;
		}
	})
};


// Route for user registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		password : reqBody.password,

		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user,error) => {

		
		if(error){
			return false;
		
		} else {
			return true;
		}
	})
}

// Route for the user authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {

			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) { 

				
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}

		}

	})
}

// Retrieve user details
module.exports.getDetails =(reqBody) => {
	return User.findById(reqBody.userId).then(result => {


		result.password = "";

		return result;
	});
	
};	


module.exports.createOrder = async (data) => {
  let isUserUpdated = await User.findById(data.productName);
  let isProductUpdated = await Product.findById(data.orderId);
  let isTotalAmount = await User.findById(data.totalAmount);
  let isCheckOut = isProductUpdated.orders.find(
    (user) => user.orderId == data.orderId );
  
  if (isUserUpdated && isProductUpdated && isTotalAmount && !isCheckOut) {
		isUserUpdated.order.push({ productName: data.productName });
		isUserUpdated.save();
		isProductUpdated.orders.push({ orderId: data.orderId });
		isProductUpdated.save();
		isTotalAmount.order.push({ totalAmount: data.totalAmount });
		isTotalAmount.save();	
    return true;
  } else {
    return false;
  }
};


module.exports.productOrder = async (data) => {
 
  let isProductUpdated = await Product.findById(data.productId);
  isProductUpdated.orders.push({ userId: data.userId });

  let isUserUpdated = await User.findById(data.userId);

  let totalAmount = isProductUpdated.price * data.quantity;
  
  let orderDetails = {
    products: {
      productName: isProductUpdated.name,
      quantity: data.quantity,
    },
    totalAmount: totalAmount,
  };
  
  isUserUpdated.orders.push(orderDetails);

  if (isUserUpdated && isProductUpdated) {
    await isProductUpdated.save();
    await isUserUpdated.save();
    return true;
  } else {
    return false;
  }
};

