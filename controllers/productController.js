const Product = require("../models/Product");

// Create Product (Admin Only)
module.exports.addProduct = (data) => {
	let newProduct = new Product ({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price,
	});

	return newProduct.save().then((product, error) =>{

		if(error){
			return false;
		} else {
			return true;
		}
	})

}

// Retrieve all products

module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all active products
module.exports.getAllActiveProduct = () => {
	return Product.find({isActive:true}).then(result => {
		return result
	})
}

// Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}


// Update Product Information (Admin Only)
module.exports.updateProduct = (reqParams, reqBody) => {
	
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {


		if (error){
			return false;
		} else {
			return true;
		}
	})
}

// Archive Product (Admin Only)
module.exports.archiveProduct = (reqParams, reqBody) => { 

	let updateActiveField = {
		isActive : reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product,error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	})
};


