const express = require("express");
const auth = require("../auth");
const router = express.Router();
const userController = require("../controllers/userController");

// Route for check email
router.post("/checkEmail", (req, res) => {

  // .then method uses the result from the controller function and sends it back to the frontend application via res.send method
  userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
});

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});

// Route for the user authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
});

// Retrieve user details

router.get("/details", auth.verify, (req, res) => {

	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	userController.getDetails({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

router.post("/checkOut", auth.verify, (req, res) => {
  let user = auth.decode(req.headers.authorization);

  let product = {
    userId: user.id,
    productId: req.body.productId,
    quantity: req.body.quantity,
  };

  if (user.isAdmin === false) {
    userController
      .productOrder(product)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.send(false);
  }
});

module.exports = router;

