const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Create Product (Admin Only)
router.post("/newProduct", auth.verify, (req,res) => {

const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin){

		productController.addProduct(data).then(resultFromController => res.send(resultFromController))		
	
	} else {
		res.send(false)
	}
	
});

// Retrieving all products
router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Retrieve all active products
router.get("/allActive", (req,res) => {
	productController.getAllActiveProduct().then(resultFromController => res.send (resultFromController));
})

// Retrieve specific product
router.get("/:productId", (req,res) => {
	productController.getProduct(req.params).then(resultFromController => res.send (resultFromController));
});

// Update Product Information (Admin Only)
router.put("/:productId", auth.verify, (req,res) => {

		const data = auth.decode(req.headers.authorization);

		if(data.isAdmin){
			productController.updateProduct(req.params, req.body).then(resultFromController => res.send (resultFromController));
		} else {
			res.send(false)
		}
	
});



// Archive Product (Admin Only)
router.patch("/:productId/archive", auth.verify, (req,res) => {
	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

})


module.exports = router;