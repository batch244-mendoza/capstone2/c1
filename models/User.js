const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(

		{
			email: {
				type: String,
				required: [true, "Email is required"]
			},
			password: {
				type: String,
				required: [true, "Password is required"]
			},
			isAdmin: {
				type: Boolean,
				default: false
			},
			orders: [
			{
				products: [ 
					{
						_id: false,
						productName:{
							type:String,
							required: [true, "ProductName is required."],	
						},
						quantity: {
							type:Number,
							required: [true, "Quantity is required."],
						},
					
					},
						],

				totalAmount: {
					type: Number,
					required: [true, "Total Amount is required."], 
				},
				purchasedOn: {
					type: Date,
					default: new Date(),
				},

			},

			]


	     }
)

module.exports = mongoose.model("User", userSchema)